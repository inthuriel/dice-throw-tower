﻿
namespace diceShuffler
{
    partial class DiceThrowTower
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiceThrowTower));
            this.diceBox = new System.Windows.Forms.TableLayoutPanel();
            this.d100Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d100DiceNumber = new System.Windows.Forms.Label();
            this.d100DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d100NumberPlus = new System.Windows.Forms.Label();
            this.d100NumberMinus = new System.Windows.Forms.Label();
            this.d100 = new System.Windows.Forms.Button();
            this.d20Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d20DiceNumber = new System.Windows.Forms.Label();
            this.d20DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d20NumberPlus = new System.Windows.Forms.Label();
            this.d20NumberMinus = new System.Windows.Forms.Label();
            this.d20 = new System.Windows.Forms.Button();
            this.d8Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d8DiceNumber = new System.Windows.Forms.Label();
            this.d8DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d8NumberPlus = new System.Windows.Forms.Label();
            this.d8NumberMinus = new System.Windows.Forms.Label();
            this.d8 = new System.Windows.Forms.Button();
            this.d10Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d10DiceNumber = new System.Windows.Forms.Label();
            this.d10DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d10NumberPlus = new System.Windows.Forms.Label();
            this.d10NumberMinus = new System.Windows.Forms.Label();
            this.d10 = new System.Windows.Forms.Button();
            this.d12Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d12DiceNumber = new System.Windows.Forms.Label();
            this.d12DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d12NumberPlus = new System.Windows.Forms.Label();
            this.d12NumberMinus = new System.Windows.Forms.Label();
            this.d12 = new System.Windows.Forms.Button();
            this.d6Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d6 = new System.Windows.Forms.Button();
            this.d6DiceNumber = new System.Windows.Forms.Label();
            this.d6DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d6NumberPlus = new System.Windows.Forms.Label();
            this.d6NumberMinus = new System.Windows.Forms.Label();
            this.throwBox = new System.Windows.Forms.GroupBox();
            this.throwDicePanel = new System.Windows.Forms.TableLayoutPanel();
            this.throwDiceButton = new System.Windows.Forms.Button();
            this.throwNumberButtons = new System.Windows.Forms.TableLayoutPanel();
            this.throwNumberPlus = new System.Windows.Forms.Button();
            this.throwNumberMinus = new System.Windows.Forms.Button();
            this.throwsNumber = new System.Windows.Forms.Label();
            this.d4Panel = new System.Windows.Forms.TableLayoutPanel();
            this.d4 = new System.Windows.Forms.Button();
            this.d4DiceNumber = new System.Windows.Forms.Label();
            this.d4DiceNumberChange = new System.Windows.Forms.TableLayoutPanel();
            this.d4NumberPlus = new System.Windows.Forms.Label();
            this.d4NumberMinus = new System.Windows.Forms.Label();
            this.spine = new System.Windows.Forms.TableLayoutPanel();
            this.savedThrowsGroup = new System.Windows.Forms.GroupBox();
            this.lastThrows = new System.Windows.Forms.ListView();
            this.throwResultsTable = new System.Windows.Forms.TableLayoutPanel();
            this.throwResultsPlaceholder = new System.Windows.Forms.Panel();
            this.throwResultHeader = new System.Windows.Forms.Label();
            this.thrownDice = new System.Windows.Forms.Label();
            this.throwWithResultsPlaceholder = new System.Windows.Forms.Label();
            this.authorInfo = new System.Windows.Forms.Label();
            this.diceBox.SuspendLayout();
            this.d100Panel.SuspendLayout();
            this.d100DiceNumberChange.SuspendLayout();
            this.d20Panel.SuspendLayout();
            this.d20DiceNumberChange.SuspendLayout();
            this.d8Panel.SuspendLayout();
            this.d8DiceNumberChange.SuspendLayout();
            this.d10Panel.SuspendLayout();
            this.d10DiceNumberChange.SuspendLayout();
            this.d12Panel.SuspendLayout();
            this.d12DiceNumberChange.SuspendLayout();
            this.d6Panel.SuspendLayout();
            this.d6DiceNumberChange.SuspendLayout();
            this.throwBox.SuspendLayout();
            this.throwDicePanel.SuspendLayout();
            this.throwNumberButtons.SuspendLayout();
            this.d4Panel.SuspendLayout();
            this.d4DiceNumberChange.SuspendLayout();
            this.spine.SuspendLayout();
            this.savedThrowsGroup.SuspendLayout();
            this.throwResultsTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // diceBox
            // 
            this.diceBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.diceBox.ColumnCount = 2;
            this.diceBox.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.diceBox.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.diceBox.Controls.Add(this.d100Panel, 0, 3);
            this.diceBox.Controls.Add(this.d20Panel, 1, 2);
            this.diceBox.Controls.Add(this.d8Panel, 0, 2);
            this.diceBox.Controls.Add(this.d10Panel, 1, 1);
            this.diceBox.Controls.Add(this.d12Panel, 0, 1);
            this.diceBox.Controls.Add(this.d6Panel, 1, 0);
            this.diceBox.Controls.Add(this.throwBox, 1, 3);
            this.diceBox.Controls.Add(this.d4Panel, 0, 0);
            this.diceBox.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.diceBox.Location = new System.Drawing.Point(3, 3);
            this.diceBox.MinimumSize = new System.Drawing.Size(70, 70);
            this.diceBox.Name = "diceBox";
            this.diceBox.RowCount = 4;
            this.diceBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.diceBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.diceBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.diceBox.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.diceBox.Size = new System.Drawing.Size(460, 485);
            this.diceBox.TabIndex = 8;
            // 
            // d100Panel
            // 
            this.d100Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d100Panel.ColumnCount = 3;
            this.d100Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d100Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d100Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d100Panel.Controls.Add(this.d100DiceNumber, 2, 0);
            this.d100Panel.Controls.Add(this.d100DiceNumberChange, 1, 0);
            this.d100Panel.Controls.Add(this.d100, 0, 0);
            this.d100Panel.Location = new System.Drawing.Point(3, 366);
            this.d100Panel.Name = "d100Panel";
            this.d100Panel.RowCount = 1;
            this.d100Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d100Panel.Size = new System.Drawing.Size(224, 116);
            this.d100Panel.TabIndex = 14;
            // 
            // d100DiceNumber
            // 
            this.d100DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d100DiceNumber.AutoSize = true;
            this.d100DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d100DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d100DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d100DiceNumber.Name = "d100DiceNumber";
            this.d100DiceNumber.Size = new System.Drawing.Size(61, 116);
            this.d100DiceNumber.TabIndex = 1;
            this.d100DiceNumber.Text = "1";
            this.d100DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d100DiceNumberChange
            // 
            this.d100DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d100DiceNumberChange.ColumnCount = 1;
            this.d100DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d100DiceNumberChange.Controls.Add(this.d100NumberPlus, 0, 0);
            this.d100DiceNumberChange.Controls.Add(this.d100NumberMinus, 0, 1);
            this.d100DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d100DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d100DiceNumberChange.Name = "d100DiceNumberChange";
            this.d100DiceNumberChange.RowCount = 2;
            this.d100DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d100DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d100DiceNumberChange.Size = new System.Drawing.Size(38, 110);
            this.d100DiceNumberChange.TabIndex = 2;
            // 
            // d100NumberPlus
            // 
            this.d100NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d100NumberPlus.AutoSize = true;
            this.d100NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d100NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d100NumberPlus.Name = "d100NumberPlus";
            this.d100NumberPlus.Size = new System.Drawing.Size(32, 55);
            this.d100NumberPlus.TabIndex = 0;
            this.d100NumberPlus.Text = "+";
            this.d100NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d100NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d100NumberMinus
            // 
            this.d100NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d100NumberMinus.AutoSize = true;
            this.d100NumberMinus.Location = new System.Drawing.Point(3, 55);
            this.d100NumberMinus.Name = "d100NumberMinus";
            this.d100NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d100NumberMinus.TabIndex = 1;
            this.d100NumberMinus.Text = "-";
            this.d100NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d100NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d100
            // 
            this.d100.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d100.FlatAppearance.BorderSize = 0;
            this.d100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d100.Image = global::diceShuffler.Properties.Resources.d100;
            this.d100.Location = new System.Drawing.Point(10, 10);
            this.d100.Margin = new System.Windows.Forms.Padding(10);
            this.d100.Name = "d100";
            this.d100.Size = new System.Drawing.Size(93, 96);
            this.d100.TabIndex = 6;
            this.d100.UseVisualStyleBackColor = true;
            this.d100.Click += new System.EventHandler(this.d100_Click);
            // 
            // d20Panel
            // 
            this.d20Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d20Panel.ColumnCount = 3;
            this.d20Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d20Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d20Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d20Panel.Controls.Add(this.d20DiceNumber, 2, 0);
            this.d20Panel.Controls.Add(this.d20DiceNumberChange, 1, 0);
            this.d20Panel.Controls.Add(this.d20, 0, 0);
            this.d20Panel.Location = new System.Drawing.Point(233, 245);
            this.d20Panel.Name = "d20Panel";
            this.d20Panel.RowCount = 1;
            this.d20Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d20Panel.Size = new System.Drawing.Size(224, 115);
            this.d20Panel.TabIndex = 13;
            // 
            // d20DiceNumber
            // 
            this.d20DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d20DiceNumber.AutoSize = true;
            this.d20DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d20DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d20DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d20DiceNumber.Name = "d20DiceNumber";
            this.d20DiceNumber.Size = new System.Drawing.Size(61, 115);
            this.d20DiceNumber.TabIndex = 1;
            this.d20DiceNumber.Text = "1";
            this.d20DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d20DiceNumberChange
            // 
            this.d20DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d20DiceNumberChange.ColumnCount = 1;
            this.d20DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d20DiceNumberChange.Controls.Add(this.d20NumberPlus, 0, 0);
            this.d20DiceNumberChange.Controls.Add(this.d20NumberMinus, 0, 1);
            this.d20DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d20DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d20DiceNumberChange.Name = "d20DiceNumberChange";
            this.d20DiceNumberChange.RowCount = 2;
            this.d20DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d20DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d20DiceNumberChange.Size = new System.Drawing.Size(38, 109);
            this.d20DiceNumberChange.TabIndex = 2;
            // 
            // d20NumberPlus
            // 
            this.d20NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d20NumberPlus.AutoSize = true;
            this.d20NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d20NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d20NumberPlus.Name = "d20NumberPlus";
            this.d20NumberPlus.Size = new System.Drawing.Size(32, 54);
            this.d20NumberPlus.TabIndex = 0;
            this.d20NumberPlus.Text = "+";
            this.d20NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d20NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d20NumberMinus
            // 
            this.d20NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d20NumberMinus.AutoSize = true;
            this.d20NumberMinus.Location = new System.Drawing.Point(3, 54);
            this.d20NumberMinus.Name = "d20NumberMinus";
            this.d20NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d20NumberMinus.TabIndex = 1;
            this.d20NumberMinus.Text = "-";
            this.d20NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d20NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d20
            // 
            this.d20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d20.FlatAppearance.BorderSize = 0;
            this.d20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d20.Image = global::diceShuffler.Properties.Resources.d20;
            this.d20.Location = new System.Drawing.Point(10, 10);
            this.d20.Margin = new System.Windows.Forms.Padding(10);
            this.d20.Name = "d20";
            this.d20.Size = new System.Drawing.Size(93, 95);
            this.d20.TabIndex = 5;
            this.d20.UseVisualStyleBackColor = true;
            this.d20.Click += new System.EventHandler(this.d20_Click);
            // 
            // d8Panel
            // 
            this.d8Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d8Panel.ColumnCount = 3;
            this.d8Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d8Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d8Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d8Panel.Controls.Add(this.d8DiceNumber, 2, 0);
            this.d8Panel.Controls.Add(this.d8DiceNumberChange, 1, 0);
            this.d8Panel.Controls.Add(this.d8, 0, 0);
            this.d8Panel.Location = new System.Drawing.Point(3, 245);
            this.d8Panel.Name = "d8Panel";
            this.d8Panel.RowCount = 1;
            this.d8Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d8Panel.Size = new System.Drawing.Size(224, 115);
            this.d8Panel.TabIndex = 12;
            // 
            // d8DiceNumber
            // 
            this.d8DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d8DiceNumber.AutoSize = true;
            this.d8DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d8DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d8DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d8DiceNumber.Name = "d8DiceNumber";
            this.d8DiceNumber.Size = new System.Drawing.Size(61, 115);
            this.d8DiceNumber.TabIndex = 1;
            this.d8DiceNumber.Text = "1";
            this.d8DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d8DiceNumberChange
            // 
            this.d8DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d8DiceNumberChange.ColumnCount = 1;
            this.d8DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d8DiceNumberChange.Controls.Add(this.d8NumberPlus, 0, 0);
            this.d8DiceNumberChange.Controls.Add(this.d8NumberMinus, 0, 1);
            this.d8DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d8DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d8DiceNumberChange.Name = "d8DiceNumberChange";
            this.d8DiceNumberChange.RowCount = 2;
            this.d8DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d8DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d8DiceNumberChange.Size = new System.Drawing.Size(38, 109);
            this.d8DiceNumberChange.TabIndex = 2;
            // 
            // d8NumberPlus
            // 
            this.d8NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d8NumberPlus.AutoSize = true;
            this.d8NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d8NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d8NumberPlus.Name = "d8NumberPlus";
            this.d8NumberPlus.Size = new System.Drawing.Size(32, 54);
            this.d8NumberPlus.TabIndex = 0;
            this.d8NumberPlus.Text = "+";
            this.d8NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d8NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d8NumberMinus
            // 
            this.d8NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d8NumberMinus.AutoSize = true;
            this.d8NumberMinus.Location = new System.Drawing.Point(3, 54);
            this.d8NumberMinus.Name = "d8NumberMinus";
            this.d8NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d8NumberMinus.TabIndex = 1;
            this.d8NumberMinus.Text = "-";
            this.d8NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d8NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d8
            // 
            this.d8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.d8.Cursor = System.Windows.Forms.Cursors.Default;
            this.d8.FlatAppearance.BorderSize = 0;
            this.d8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d8.Image = global::diceShuffler.Properties.Resources.d8;
            this.d8.Location = new System.Drawing.Point(10, 10);
            this.d8.Margin = new System.Windows.Forms.Padding(10);
            this.d8.Name = "d8";
            this.d8.Size = new System.Drawing.Size(93, 95);
            this.d8.TabIndex = 2;
            this.d8.UseVisualStyleBackColor = true;
            this.d8.Click += new System.EventHandler(this.d8_Click);
            // 
            // d10Panel
            // 
            this.d10Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d10Panel.ColumnCount = 3;
            this.d10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d10Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d10Panel.Controls.Add(this.d10DiceNumber, 2, 0);
            this.d10Panel.Controls.Add(this.d10DiceNumberChange, 1, 0);
            this.d10Panel.Controls.Add(this.d10, 0, 0);
            this.d10Panel.Location = new System.Drawing.Point(233, 124);
            this.d10Panel.Name = "d10Panel";
            this.d10Panel.RowCount = 1;
            this.d10Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d10Panel.Size = new System.Drawing.Size(224, 115);
            this.d10Panel.TabIndex = 11;
            // 
            // d10DiceNumber
            // 
            this.d10DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d10DiceNumber.AutoSize = true;
            this.d10DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d10DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d10DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d10DiceNumber.Name = "d10DiceNumber";
            this.d10DiceNumber.Size = new System.Drawing.Size(61, 115);
            this.d10DiceNumber.TabIndex = 1;
            this.d10DiceNumber.Text = "1";
            this.d10DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d10DiceNumberChange
            // 
            this.d10DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d10DiceNumberChange.ColumnCount = 1;
            this.d10DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d10DiceNumberChange.Controls.Add(this.d10NumberPlus, 0, 0);
            this.d10DiceNumberChange.Controls.Add(this.d10NumberMinus, 0, 1);
            this.d10DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d10DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d10DiceNumberChange.Name = "d10DiceNumberChange";
            this.d10DiceNumberChange.RowCount = 2;
            this.d10DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d10DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d10DiceNumberChange.Size = new System.Drawing.Size(38, 109);
            this.d10DiceNumberChange.TabIndex = 2;
            // 
            // d10NumberPlus
            // 
            this.d10NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d10NumberPlus.AutoSize = true;
            this.d10NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d10NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d10NumberPlus.Name = "d10NumberPlus";
            this.d10NumberPlus.Size = new System.Drawing.Size(32, 54);
            this.d10NumberPlus.TabIndex = 0;
            this.d10NumberPlus.Text = "+";
            this.d10NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d10NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d10NumberMinus
            // 
            this.d10NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d10NumberMinus.AutoSize = true;
            this.d10NumberMinus.Location = new System.Drawing.Point(3, 54);
            this.d10NumberMinus.Name = "d10NumberMinus";
            this.d10NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d10NumberMinus.TabIndex = 1;
            this.d10NumberMinus.Text = "-";
            this.d10NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d10NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d10
            // 
            this.d10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d10.FlatAppearance.BorderSize = 0;
            this.d10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d10.Image = global::diceShuffler.Properties.Resources.d10;
            this.d10.Location = new System.Drawing.Point(10, 10);
            this.d10.Margin = new System.Windows.Forms.Padding(10);
            this.d10.Name = "d10";
            this.d10.Size = new System.Drawing.Size(93, 95);
            this.d10.TabIndex = 3;
            this.d10.UseVisualStyleBackColor = true;
            this.d10.Click += new System.EventHandler(this.d10_Click);
            // 
            // d12Panel
            // 
            this.d12Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d12Panel.ColumnCount = 3;
            this.d12Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d12Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d12Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d12Panel.Controls.Add(this.d12DiceNumber, 2, 0);
            this.d12Panel.Controls.Add(this.d12DiceNumberChange, 1, 0);
            this.d12Panel.Controls.Add(this.d12, 0, 0);
            this.d12Panel.Location = new System.Drawing.Point(3, 124);
            this.d12Panel.Name = "d12Panel";
            this.d12Panel.RowCount = 1;
            this.d12Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d12Panel.Size = new System.Drawing.Size(224, 115);
            this.d12Panel.TabIndex = 10;
            // 
            // d12DiceNumber
            // 
            this.d12DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d12DiceNumber.AutoSize = true;
            this.d12DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d12DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d12DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d12DiceNumber.Name = "d12DiceNumber";
            this.d12DiceNumber.Size = new System.Drawing.Size(61, 115);
            this.d12DiceNumber.TabIndex = 1;
            this.d12DiceNumber.Text = "1";
            this.d12DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d12DiceNumberChange
            // 
            this.d12DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d12DiceNumberChange.ColumnCount = 1;
            this.d12DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d12DiceNumberChange.Controls.Add(this.d12NumberPlus, 0, 0);
            this.d12DiceNumberChange.Controls.Add(this.d12NumberMinus, 0, 1);
            this.d12DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d12DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d12DiceNumberChange.Name = "d12DiceNumberChange";
            this.d12DiceNumberChange.RowCount = 2;
            this.d12DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d12DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d12DiceNumberChange.Size = new System.Drawing.Size(38, 109);
            this.d12DiceNumberChange.TabIndex = 2;
            // 
            // d12NumberPlus
            // 
            this.d12NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d12NumberPlus.AutoSize = true;
            this.d12NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d12NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d12NumberPlus.Name = "d12NumberPlus";
            this.d12NumberPlus.Size = new System.Drawing.Size(32, 54);
            this.d12NumberPlus.TabIndex = 0;
            this.d12NumberPlus.Text = "+";
            this.d12NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d12NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d12NumberMinus
            // 
            this.d12NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d12NumberMinus.AutoSize = true;
            this.d12NumberMinus.Location = new System.Drawing.Point(3, 54);
            this.d12NumberMinus.Name = "d12NumberMinus";
            this.d12NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d12NumberMinus.TabIndex = 1;
            this.d12NumberMinus.Text = "-";
            this.d12NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d12NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d12
            // 
            this.d12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.d12.FlatAppearance.BorderSize = 0;
            this.d12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d12.Image = global::diceShuffler.Properties.Resources.d12;
            this.d12.Location = new System.Drawing.Point(10, 10);
            this.d12.Margin = new System.Windows.Forms.Padding(10);
            this.d12.Name = "d12";
            this.d12.Size = new System.Drawing.Size(93, 95);
            this.d12.TabIndex = 4;
            this.d12.UseVisualStyleBackColor = true;
            this.d12.Click += new System.EventHandler(this.d12_Click);
            // 
            // d6Panel
            // 
            this.d6Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d6Panel.ColumnCount = 3;
            this.d6Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d6Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d6Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d6Panel.Controls.Add(this.d6, 0, 0);
            this.d6Panel.Controls.Add(this.d6DiceNumber, 2, 0);
            this.d6Panel.Controls.Add(this.d6DiceNumberChange, 1, 0);
            this.d6Panel.Location = new System.Drawing.Point(233, 3);
            this.d6Panel.Name = "d6Panel";
            this.d6Panel.RowCount = 1;
            this.d6Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d6Panel.Size = new System.Drawing.Size(224, 115);
            this.d6Panel.TabIndex = 9;
            // 
            // d6
            // 
            this.d6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d6.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.d6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.d6.FlatAppearance.BorderSize = 0;
            this.d6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d6.Image = global::diceShuffler.Properties.Resources.d6;
            this.d6.Location = new System.Drawing.Point(10, 10);
            this.d6.Margin = new System.Windows.Forms.Padding(10);
            this.d6.Name = "d6";
            this.d6.Size = new System.Drawing.Size(93, 95);
            this.d6.TabIndex = 1;
            this.d6.UseVisualStyleBackColor = false;
            this.d6.Click += new System.EventHandler(this.d6_Click);
            // 
            // d6DiceNumber
            // 
            this.d6DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d6DiceNumber.AutoSize = true;
            this.d6DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d6DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d6DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d6DiceNumber.Name = "d6DiceNumber";
            this.d6DiceNumber.Size = new System.Drawing.Size(61, 115);
            this.d6DiceNumber.TabIndex = 1;
            this.d6DiceNumber.Text = "1";
            this.d6DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d6DiceNumberChange
            // 
            this.d6DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d6DiceNumberChange.ColumnCount = 1;
            this.d6DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d6DiceNumberChange.Controls.Add(this.d6NumberPlus, 0, 0);
            this.d6DiceNumberChange.Controls.Add(this.d6NumberMinus, 0, 1);
            this.d6DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d6DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d6DiceNumberChange.Name = "d6DiceNumberChange";
            this.d6DiceNumberChange.RowCount = 2;
            this.d6DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d6DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d6DiceNumberChange.Size = new System.Drawing.Size(38, 109);
            this.d6DiceNumberChange.TabIndex = 2;
            // 
            // d6NumberPlus
            // 
            this.d6NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d6NumberPlus.AutoSize = true;
            this.d6NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d6NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d6NumberPlus.Name = "d6NumberPlus";
            this.d6NumberPlus.Size = new System.Drawing.Size(32, 54);
            this.d6NumberPlus.TabIndex = 0;
            this.d6NumberPlus.Text = "+";
            this.d6NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d6NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d6NumberMinus
            // 
            this.d6NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d6NumberMinus.AutoSize = true;
            this.d6NumberMinus.Location = new System.Drawing.Point(3, 54);
            this.d6NumberMinus.Name = "d6NumberMinus";
            this.d6NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d6NumberMinus.TabIndex = 1;
            this.d6NumberMinus.Text = "-";
            this.d6NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d6NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // throwBox
            // 
            this.throwBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwBox.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.throwBox.Controls.Add(this.throwDicePanel);
            this.throwBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.throwBox.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.throwBox.ForeColor = System.Drawing.Color.White;
            this.throwBox.Location = new System.Drawing.Point(233, 366);
            this.throwBox.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.throwBox.Name = "throwBox";
            this.throwBox.Size = new System.Drawing.Size(224, 119);
            this.throwBox.TabIndex = 7;
            this.throwBox.TabStop = false;
            this.throwBox.Text = "Throw";
            // 
            // throwDicePanel
            // 
            this.throwDicePanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwDicePanel.ColumnCount = 3;
            this.throwDicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.throwDicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.throwDicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.throwDicePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.throwDicePanel.Controls.Add(this.throwDiceButton, 2, 0);
            this.throwDicePanel.Controls.Add(this.throwNumberButtons, 0, 0);
            this.throwDicePanel.Controls.Add(this.throwsNumber, 1, 0);
            this.throwDicePanel.Location = new System.Drawing.Point(6, 31);
            this.throwDicePanel.Name = "throwDicePanel";
            this.throwDicePanel.RowCount = 1;
            this.throwDicePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.throwDicePanel.Size = new System.Drawing.Size(210, 67);
            this.throwDicePanel.TabIndex = 10;
            // 
            // throwDiceButton
            // 
            this.throwDiceButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwDiceButton.FlatAppearance.BorderSize = 0;
            this.throwDiceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.throwDiceButton.Image = global::diceShuffler.Properties.Resources.rolling_dices;
            this.throwDiceButton.Location = new System.Drawing.Point(147, 3);
            this.throwDiceButton.Name = "throwDiceButton";
            this.throwDiceButton.Size = new System.Drawing.Size(60, 61);
            this.throwDiceButton.TabIndex = 5;
            this.throwDiceButton.UseVisualStyleBackColor = true;
            this.throwDiceButton.Click += new System.EventHandler(this.throwDiceButton_Click);
            // 
            // throwNumberButtons
            // 
            this.throwNumberButtons.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwNumberButtons.ColumnCount = 1;
            this.throwNumberButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.throwNumberButtons.Controls.Add(this.throwNumberPlus, 0, 0);
            this.throwNumberButtons.Controls.Add(this.throwNumberMinus, 0, 1);
            this.throwNumberButtons.Location = new System.Drawing.Point(3, 3);
            this.throwNumberButtons.Name = "throwNumberButtons";
            this.throwNumberButtons.RowCount = 2;
            this.throwNumberButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.throwNumberButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.throwNumberButtons.Size = new System.Drawing.Size(29, 61);
            this.throwNumberButtons.TabIndex = 9;
            // 
            // throwNumberPlus
            // 
            this.throwNumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwNumberPlus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.throwNumberPlus.FlatAppearance.BorderSize = 0;
            this.throwNumberPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.throwNumberPlus.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.throwNumberPlus.ForeColor = System.Drawing.Color.White;
            this.throwNumberPlus.Location = new System.Drawing.Point(0, 0);
            this.throwNumberPlus.Margin = new System.Windows.Forms.Padding(0);
            this.throwNumberPlus.Name = "throwNumberPlus";
            this.throwNumberPlus.Size = new System.Drawing.Size(29, 30);
            this.throwNumberPlus.TabIndex = 7;
            this.throwNumberPlus.Text = "+";
            this.throwNumberPlus.UseVisualStyleBackColor = true;
            this.throwNumberPlus.Click += new System.EventHandler(this.throwNumberPlus_Click);
            // 
            // throwNumberMinus
            // 
            this.throwNumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwNumberMinus.FlatAppearance.BorderSize = 0;
            this.throwNumberMinus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.throwNumberMinus.Font = new System.Drawing.Font("Courier New", 18F, System.Drawing.FontStyle.Bold);
            this.throwNumberMinus.ForeColor = System.Drawing.Color.White;
            this.throwNumberMinus.Location = new System.Drawing.Point(0, 30);
            this.throwNumberMinus.Margin = new System.Windows.Forms.Padding(0);
            this.throwNumberMinus.Name = "throwNumberMinus";
            this.throwNumberMinus.Size = new System.Drawing.Size(29, 31);
            this.throwNumberMinus.TabIndex = 8;
            this.throwNumberMinus.Text = "-";
            this.throwNumberMinus.UseVisualStyleBackColor = true;
            this.throwNumberMinus.Click += new System.EventHandler(this.throwNumberMinus_Click);
            // 
            // throwsNumber
            // 
            this.throwsNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwsNumber.AutoSize = true;
            this.throwsNumber.Font = new System.Drawing.Font("Verdana", 38F, System.Drawing.FontStyle.Bold);
            this.throwsNumber.ForeColor = System.Drawing.Color.Peru;
            this.throwsNumber.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.throwsNumber.Location = new System.Drawing.Point(38, 0);
            this.throwsNumber.Name = "throwsNumber";
            this.throwsNumber.Size = new System.Drawing.Size(99, 67);
            this.throwsNumber.TabIndex = 13;
            this.throwsNumber.Text = "1";
            this.throwsNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d4Panel
            // 
            this.d4Panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d4Panel.ColumnCount = 3;
            this.d4Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 71.81208F));
            this.d4Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.18792F));
            this.d4Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.d4Panel.Controls.Add(this.d4, 0, 0);
            this.d4Panel.Controls.Add(this.d4DiceNumber, 2, 0);
            this.d4Panel.Controls.Add(this.d4DiceNumberChange, 1, 0);
            this.d4Panel.Location = new System.Drawing.Point(3, 3);
            this.d4Panel.Name = "d4Panel";
            this.d4Panel.RowCount = 1;
            this.d4Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d4Panel.Size = new System.Drawing.Size(224, 115);
            this.d4Panel.TabIndex = 8;
            // 
            // d4
            // 
            this.d4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d4.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.d4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.d4.FlatAppearance.BorderSize = 0;
            this.d4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.d4.Image = ((System.Drawing.Image)(resources.GetObject("d4.Image")));
            this.d4.Location = new System.Drawing.Point(10, 10);
            this.d4.Margin = new System.Windows.Forms.Padding(10);
            this.d4.Name = "d4";
            this.d4.Size = new System.Drawing.Size(93, 95);
            this.d4.TabIndex = 0;
            this.d4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.d4.UseVisualStyleBackColor = false;
            this.d4.Click += new System.EventHandler(this.d4_Click);
            // 
            // d4DiceNumber
            // 
            this.d4DiceNumber.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d4DiceNumber.AutoSize = true;
            this.d4DiceNumber.Font = new System.Drawing.Font("Verdana", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d4DiceNumber.ForeColor = System.Drawing.Color.Peru;
            this.d4DiceNumber.Location = new System.Drawing.Point(160, 0);
            this.d4DiceNumber.Name = "d4DiceNumber";
            this.d4DiceNumber.Size = new System.Drawing.Size(61, 115);
            this.d4DiceNumber.TabIndex = 1;
            this.d4DiceNumber.Text = "1";
            this.d4DiceNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // d4DiceNumberChange
            // 
            this.d4DiceNumberChange.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d4DiceNumberChange.ColumnCount = 1;
            this.d4DiceNumberChange.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d4DiceNumberChange.Controls.Add(this.d4NumberPlus, 0, 0);
            this.d4DiceNumberChange.Controls.Add(this.d4NumberMinus, 0, 1);
            this.d4DiceNumberChange.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold);
            this.d4DiceNumberChange.Location = new System.Drawing.Point(116, 3);
            this.d4DiceNumberChange.Name = "d4DiceNumberChange";
            this.d4DiceNumberChange.RowCount = 2;
            this.d4DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d4DiceNumberChange.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.d4DiceNumberChange.Size = new System.Drawing.Size(38, 109);
            this.d4DiceNumberChange.TabIndex = 2;
            // 
            // d4NumberPlus
            // 
            this.d4NumberPlus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d4NumberPlus.AutoSize = true;
            this.d4NumberPlus.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.d4NumberPlus.Location = new System.Drawing.Point(3, 0);
            this.d4NumberPlus.Name = "d4NumberPlus";
            this.d4NumberPlus.Size = new System.Drawing.Size(32, 54);
            this.d4NumberPlus.TabIndex = 0;
            this.d4NumberPlus.Text = "+";
            this.d4NumberPlus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d4NumberPlus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // d4NumberMinus
            // 
            this.d4NumberMinus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.d4NumberMinus.AutoSize = true;
            this.d4NumberMinus.Location = new System.Drawing.Point(3, 54);
            this.d4NumberMinus.Name = "d4NumberMinus";
            this.d4NumberMinus.Size = new System.Drawing.Size(32, 55);
            this.d4NumberMinus.TabIndex = 1;
            this.d4NumberMinus.Text = "-";
            this.d4NumberMinus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.d4NumberMinus.Click += new System.EventHandler(this.NumberOfDiceChange);
            // 
            // spine
            // 
            this.spine.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.spine.ColumnCount = 2;
            this.spine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.spine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 394F));
            this.spine.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.spine.Controls.Add(this.savedThrowsGroup, 1, 0);
            this.spine.Controls.Add(this.throwResultsTable, 0, 1);
            this.spine.Controls.Add(this.authorInfo, 1, 2);
            this.spine.Controls.Add(this.diceBox, 0, 0);
            this.spine.ForeColor = System.Drawing.Color.White;
            this.spine.Location = new System.Drawing.Point(12, 12);
            this.spine.Name = "spine";
            this.spine.RowCount = 3;
            this.spine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 79.93827F));
            this.spine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 414F));
            this.spine.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.spine.Size = new System.Drawing.Size(860, 937);
            this.spine.TabIndex = 10;
            // 
            // savedThrowsGroup
            // 
            this.savedThrowsGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.savedThrowsGroup.Controls.Add(this.lastThrows);
            this.savedThrowsGroup.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this.savedThrowsGroup.ForeColor = System.Drawing.Color.White;
            this.savedThrowsGroup.Location = new System.Drawing.Point(469, 3);
            this.savedThrowsGroup.Name = "savedThrowsGroup";
            this.savedThrowsGroup.Size = new System.Drawing.Size(388, 485);
            this.savedThrowsGroup.TabIndex = 12;
            this.savedThrowsGroup.TabStop = false;
            this.savedThrowsGroup.Text = "Last throws";
            // 
            // lastThrows
            // 
            this.lastThrows.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lastThrows.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.lastThrows.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lastThrows.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lastThrows.ForeColor = System.Drawing.Color.Peru;
            this.lastThrows.HideSelection = false;
            this.lastThrows.Location = new System.Drawing.Point(6, 23);
            this.lastThrows.Name = "lastThrows";
            this.lastThrows.Size = new System.Drawing.Size(376, 456);
            this.lastThrows.TabIndex = 0;
            this.lastThrows.UseCompatibleStateImageBehavior = false;
            this.lastThrows.View = System.Windows.Forms.View.List;
            // 
            // throwResultsTable
            // 
            this.throwResultsTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwResultsTable.ColumnCount = 1;
            this.throwResultsTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.throwResultsTable.Controls.Add(this.throwResultsPlaceholder, 0, 3);
            this.throwResultsTable.Controls.Add(this.thrownDice, 0, 1);
            this.throwResultsTable.Controls.Add(this.throwWithResultsPlaceholder, 0, 2);
            this.throwResultsTable.Controls.Add(this.throwResultHeader, 0, 0);
            this.throwResultsTable.Location = new System.Drawing.Point(3, 494);
            this.throwResultsTable.Name = "throwResultsTable";
            this.throwResultsTable.RowCount = 4;
            this.throwResultsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 39.13044F));
            this.throwResultsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60.86956F));
            this.throwResultsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.throwResultsTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 245F));
            this.throwResultsTable.Size = new System.Drawing.Size(460, 408);
            this.throwResultsTable.TabIndex = 11;
            // 
            // throwResultsPlaceholder
            // 
            this.throwResultsPlaceholder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwResultsPlaceholder.Location = new System.Drawing.Point(3, 165);
            this.throwResultsPlaceholder.Name = "throwResultsPlaceholder";
            this.throwResultsPlaceholder.Size = new System.Drawing.Size(454, 240);
            this.throwResultsPlaceholder.TabIndex = 13;
            // 
            // throwResultHeader
            // 
            this.throwResultHeader.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwResultHeader.AutoSize = true;
            this.throwResultHeader.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this.throwResultHeader.Location = new System.Drawing.Point(3, 0);
            this.throwResultHeader.Name = "throwResultHeader";
            this.throwResultHeader.Size = new System.Drawing.Size(454, 45);
            this.throwResultHeader.TabIndex = 14;
            this.throwResultHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // thrownDice
            // 
            this.thrownDice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thrownDice.Font = new System.Drawing.Font("Verdana", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.thrownDice.ForeColor = System.Drawing.Color.Peru;
            this.thrownDice.Location = new System.Drawing.Point(3, 45);
            this.thrownDice.Name = "thrownDice";
            this.thrownDice.Size = new System.Drawing.Size(454, 70);
            this.thrownDice.TabIndex = 15;
            this.thrownDice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // throwWithResultsPlaceholder
            // 
            this.throwWithResultsPlaceholder.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.throwWithResultsPlaceholder.AutoSize = true;
            this.throwWithResultsPlaceholder.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold);
            this.throwWithResultsPlaceholder.Location = new System.Drawing.Point(3, 115);
            this.throwWithResultsPlaceholder.Name = "throwWithResultsPlaceholder";
            this.throwWithResultsPlaceholder.Size = new System.Drawing.Size(454, 47);
            this.throwWithResultsPlaceholder.TabIndex = 16;
            this.throwWithResultsPlaceholder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // authorInfo
            // 
            this.authorInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.authorInfo.AutoSize = true;
            this.authorInfo.Font = new System.Drawing.Font("Verdana", 8F, System.Drawing.FontStyle.Bold);
            this.authorInfo.Location = new System.Drawing.Point(469, 905);
            this.authorInfo.Name = "authorInfo";
            this.authorInfo.Size = new System.Drawing.Size(388, 32);
            this.authorInfo.TabIndex = 13;
            this.authorInfo.Text = "Written by Mikolaj Niedbala 2021";
            this.authorInfo.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // DiceThrowTower
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(884, 961);
            this.Controls.Add(this.spine);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(900, 1000);
            this.MinimumSize = new System.Drawing.Size(900, 1000);
            this.Name = "DiceThrowTower";
            this.ShowInTaskbar = false;
            this.Text = "Dice throw tower";
            this.diceBox.ResumeLayout(false);
            this.d100Panel.ResumeLayout(false);
            this.d100Panel.PerformLayout();
            this.d100DiceNumberChange.ResumeLayout(false);
            this.d100DiceNumberChange.PerformLayout();
            this.d20Panel.ResumeLayout(false);
            this.d20Panel.PerformLayout();
            this.d20DiceNumberChange.ResumeLayout(false);
            this.d20DiceNumberChange.PerformLayout();
            this.d8Panel.ResumeLayout(false);
            this.d8Panel.PerformLayout();
            this.d8DiceNumberChange.ResumeLayout(false);
            this.d8DiceNumberChange.PerformLayout();
            this.d10Panel.ResumeLayout(false);
            this.d10Panel.PerformLayout();
            this.d10DiceNumberChange.ResumeLayout(false);
            this.d10DiceNumberChange.PerformLayout();
            this.d12Panel.ResumeLayout(false);
            this.d12Panel.PerformLayout();
            this.d12DiceNumberChange.ResumeLayout(false);
            this.d12DiceNumberChange.PerformLayout();
            this.d6Panel.ResumeLayout(false);
            this.d6Panel.PerformLayout();
            this.d6DiceNumberChange.ResumeLayout(false);
            this.d6DiceNumberChange.PerformLayout();
            this.throwBox.ResumeLayout(false);
            this.throwDicePanel.ResumeLayout(false);
            this.throwDicePanel.PerformLayout();
            this.throwNumberButtons.ResumeLayout(false);
            this.d4Panel.ResumeLayout(false);
            this.d4Panel.PerformLayout();
            this.d4DiceNumberChange.ResumeLayout(false);
            this.d4DiceNumberChange.PerformLayout();
            this.spine.ResumeLayout(false);
            this.spine.PerformLayout();
            this.savedThrowsGroup.ResumeLayout(false);
            this.throwResultsTable.ResumeLayout(false);
            this.throwResultsTable.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel diceBox;
        private System.Windows.Forms.Button d4;
        private System.Windows.Forms.Button d6;
        private System.Windows.Forms.Button d8;
        private System.Windows.Forms.Button d10;
        private System.Windows.Forms.Button d12;
        private System.Windows.Forms.Button d20;
        private System.Windows.Forms.Button d100;
        private System.Windows.Forms.TableLayoutPanel spine;
        private System.Windows.Forms.Button throwDiceButton;
        private System.Windows.Forms.TableLayoutPanel throwDicePanel;
        private System.Windows.Forms.TableLayoutPanel throwNumberButtons;
        private System.Windows.Forms.Button throwNumberPlus;
        private System.Windows.Forms.Button throwNumberMinus;
        private System.Windows.Forms.TableLayoutPanel throwResultsTable;
        private System.Windows.Forms.GroupBox throwBox;
        private System.Windows.Forms.GroupBox savedThrowsGroup;
        private System.Windows.Forms.ListView lastThrows;
        private System.Windows.Forms.Label throwsNumber;
        private System.Windows.Forms.Panel throwResultsPlaceholder;
        private System.Windows.Forms.Label throwResultHeader;
        private System.Windows.Forms.Label thrownDice;
        private System.Windows.Forms.Label throwWithResultsPlaceholder;
        private System.Windows.Forms.Label authorInfo;
        private System.Windows.Forms.TableLayoutPanel d4Panel;
        private System.Windows.Forms.Label d4DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d4DiceNumberChange;
        private System.Windows.Forms.Label d4NumberPlus;
        private System.Windows.Forms.Label d4NumberMinus;
        private System.Windows.Forms.TableLayoutPanel d6Panel;
        private System.Windows.Forms.Label d6DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d6DiceNumberChange;
        private System.Windows.Forms.Label d6NumberPlus;
        private System.Windows.Forms.Label d6NumberMinus;
        private System.Windows.Forms.TableLayoutPanel d8Panel;
        private System.Windows.Forms.Label d8DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d8DiceNumberChange;
        private System.Windows.Forms.Label d8NumberPlus;
        private System.Windows.Forms.Label d8NumberMinus;
        private System.Windows.Forms.TableLayoutPanel d10Panel;
        private System.Windows.Forms.Label d10DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d10DiceNumberChange;
        private System.Windows.Forms.Label d10NumberPlus;
        private System.Windows.Forms.Label d10NumberMinus;
        private System.Windows.Forms.TableLayoutPanel d12Panel;
        private System.Windows.Forms.Label d12DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d12DiceNumberChange;
        private System.Windows.Forms.Label d12NumberPlus;
        private System.Windows.Forms.Label d12NumberMinus;
        private System.Windows.Forms.TableLayoutPanel d100Panel;
        private System.Windows.Forms.Label d100DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d100DiceNumberChange;
        private System.Windows.Forms.Label d100NumberPlus;
        private System.Windows.Forms.Label d100NumberMinus;
        private System.Windows.Forms.TableLayoutPanel d20Panel;
        private System.Windows.Forms.Label d20DiceNumber;
        private System.Windows.Forms.TableLayoutPanel d20DiceNumberChange;
        private System.Windows.Forms.Label d20NumberPlus;
        private System.Windows.Forms.Label d20NumberMinus;
    }
}

