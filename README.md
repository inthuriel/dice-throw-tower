# Dice throw tower

This is program allows You throw virtual dice on Your computer.  
You may chose all of `RPG-like` dice:

- `d4`
- `d6`
- `d8`
- `d10`
- `d12`
- `d20`
- `d100`

and then choose options like:

- what dice to throw (multiple choices allowed),
- number ot throws to run,
- number of dice of particular type to throw (eg. `2x d4`).

## GUI

![projectui](https://i.ibb.co/PtbwQQm/dtt.png)

## Implementation

This software is written in `C#` using `windows forms` and can be compiled at any `x86` processor.  
Software was tested in **Windows 10** environment, fell free to test and use it in other *Windows* distributions (likely **Windows 7** and above).
